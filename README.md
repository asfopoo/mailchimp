# mailChimp

tasks:

1.-completed-  Utilizes node.js 

2.-completed-  Implements the Mandrill email API (https://mandrillapp.com/api/docs/) via the Mandrill node module (https://mandrillapp.com/api/docs/index.nodejs.html)

3.-completed-  Is constructed to utilize some form of dependency injection

4.-TODO-       Is constructed to utilize a configuration file

5.-completed-  Is placed under source control using Git

6.-TODO-       Is maintained on a remote environment such as Github or Bitbucket

7.-TODO-       Is deployable to internal infrastructure

8.-TODO-       Has some level of code coverage.

9.-completed-  Utilizes PM2 as the process manager and cron scheduler.


